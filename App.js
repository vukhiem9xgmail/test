import React, { Component } from 'react';
import { View } from 'react-native';
import Home from './src/component/Home';
import PlayGame from './src/component/PlayGame';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Next from './src/component/Next';
import GameOver from './src/component/GameOver';
import Login from './src/component/Login';

const Stack = createStackNavigator();

export default class App extends Component {
  render() {
    return (
      <Login/>
    //   <NavigationContainer>
    //   <Stack.Navigator initialRouteName="Home" screenOptions={() => ({
    //       headerShown: false,
    //     })}>
    //     <Stack.Screen name="Home" component={Home}  />
    //     <Stack.Screen name="PlayGame" component={PlayGame} />
    //     <Stack.Screen name="Next" component={Next} />
    //     <Stack.Screen name="GameOver" component={GameOver} />
    //   </Stack.Navigator>
    // </NavigationContainer>
    );
  }
}