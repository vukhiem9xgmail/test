import React, { Component } from 'react';
import { View, ImageBackground, Text, TouchableOpacity, StyleSheet } from 'react-native';
import resource from '../data_/index';
import Tts from 'react-native-tts';
import AppDataManager from '../AppDataManager/AppDataManager';
var QuestionJson = require('../data_/data/hoingu.json');

const appDataManager = AppDataManager.getAppDataManagerInstance();

export default class Next extends Component {
  constructor(props) {
    super(props);
    const appData = appDataManager.getAppData();
    this.state = appData;
  }
  componentDidMount() {
    appDataManager.addObserver(this);
    this.setState({ data: this.props.route.params.data })
  }
  Next = () => {
    console.log(this.state);
    const appData = appDataManager.getAppData();
    this.state = appData;
    this.props.navigation.navigate('PlayGame');
    this.Soune();
  }
  Soune = () => {
    Tts.getInitStatus().then(() => {
      Tts.setDefaultLanguage('vi');
      Tts.setDefaultRate(0.6);
      Tts.speak(this.state.rand.cauhoi);
    });
  }
  render() {
    var listmeo = [resource.images.i_1,resource.images.i_2,resource.images.i_3,resource.images.i_4,resource.images.i_5,
      resource.images.i_6,resource.images.i_7,resource.images.i_8,resource.images.i_9,resource.images.i_10];
      var meo = Math.floor(Math.random() * 10);
    return (
      <View style={styles.container}>
        <ImageBackground source={resource.images.bghoingu} style={styles.bghoingu}>
          <ImageBackground source={resource.images.bg_khungcauhoi} style={styles.bg_khungcauhoi}>
            <Text style={styles.text_khung}>{this.state.data}</Text>
          </ImageBackground>
          <View style={styles.meo_button}>
            <ImageBackground source={listmeo[meo]} style={styles.meo}></ImageBackground>
            <TouchableOpacity
              onPress={this.Next}
              style={styles.button_start}
            >
              <Text style={styles.text_start}>Câu tiếp đê</Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    width: "100%", justifyContent: "center", alignItems: "center", marginTop: 0,
  },
  bghoingu: {
    width: '100%', height: "100%", justifyContent: "center", alignItems: "center"
  },
  bg_khungcauhoi: {
    width: 253, height: 169.8, justifyContent: "center", alignItems: "center",
  },
  text_khung: {
    fontSize: 22, color: "white",marginTop:"-15%", marginLeft:"5%"
  },
  meo_button: {
    flexDirection:"row"
  },
  meo: {
    width: 100, height: 100, justifyContent: "center", alignItems: "center", marginLeft: 0, marginTop: -20
  },
  button_start: {
    backgroundColor: "#ff4500", width: 150, height: 40, justifyContent: "center", alignItems: "center", 
    marginBottom: 10, marginTop: 10, borderRadius: 20,color: "white"
  },
  text_start: {
    color: "white",fontWeight:"600",fontFamily: "verdana"
  }
})