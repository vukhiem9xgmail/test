import React, { Component } from 'react';
import { View, ImageBackground, Text, TouchableOpacity, StyleSheet } from 'react-native';
import resource from '../data_/index';
import Tts from 'react-native-tts';
import AppDataManager from '../AppDataManager/AppDataManager';
var QuestionJson = require('../data_/data/hoingu.json');

const appDataManager = AppDataManager.getAppDataManagerInstance();


export default class GameOver extends Component {
  constructor(props) {
    super(props);
    const appData = appDataManager.getAppData();
    this.state = appData;
  }
  componentDidMount() {
    appDataManager.addObserver(this);
  }
  Start = () => {
    const appData = appDataManager.getAppData();
    this.state = appData;
    this.props.navigation.navigate('PlayGame');
    this.Soune();
  }
  Home = () => {
    this.props.navigation.navigate('Home');
  }
  Soune = () => {
    Tts.getInitStatus().then(() => {
      Tts.setDefaultLanguage('vi');
      Tts.setDefaultRate(0.6);
      Tts.speak(this.state.rand.cauhoi);
    });
  }
  render() {
    return (
      <View style={styles.container}>
        <ImageBackground source={resource.images.bghoingu} style={styles.bghoingu}>
          <Text style={styles.text_khung}>Game Over</Text>
        <TouchableOpacity
          onPress={this.Start}
          style={styles.button_start}
        >
          <Text style={styles.text_start}>Chơi lại</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={this.Home}
          style={styles.button_start}
        >
          <Text style={styles.text_start}>Home</Text>
        </TouchableOpacity>
        </ImageBackground>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    width: "100%", justifyContent: "center", alignItems: "center", marginTop: 0
  },
  bghoingu: {
    width: '100%', height: "100%", justifyContent: "center", alignItems: "center"
  },
  text_khung: {
    fontSize: 30, color: "white", marginBottom:20
  },
  button_start: {
    backgroundColor: "#ff4500", width: 150, height: 50, justifyContent: "center", alignItems: "center", 
    marginBottom: 10, marginTop: 10, borderRadius: 20,color: "white"
  },
  text_start: {
    color: "white",fontWeight:"600",fontFamily: "verdana",fontSize:20
  }
})