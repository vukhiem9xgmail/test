import React, { Component } from 'react';
import { View, StyleSheet, Text, ImageBackground } from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import resource from '../data_';

export default class Login extends Component {
    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={resource.images.bghoingu} style={styles.bghoingu}>
                    <View style={styles.up}>
                        <Text style={styles.title}>Login câu đố</Text>
                    </View>
                    <View style={styles.down}>
                        <View style={styles.textInputContainer}>
                            <TextInput
                                style={styles.textInput}
                                textContentType="emailAddress"
                                keyboardType="email-address"
                                placeholder="Enter your email"
                            >
                            </TextInput>
                        </View>
                        <View style={styles.textInputContainer}>
                            <TextInput
                                style={styles.textInput}
                                placeholder="Password"
                                secureTextEntry={true}
                            >
                            </TextInput>
                        </View>
                        <View style={styles.khung_button}>
                            <TouchableOpacity style={styles.button_dangnhap}><Text>Login</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.button_dangnhap}><Text style={styles.text_dangky}>Login width Facebook</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </ImageBackground>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    bghoingu: {
        width: '100%', height: "100%", justifyContent: "center", alignItems: "center"
    },
    up: {
        flex: 3,
        flexDirection: 'column',
    },
    down: {
        flex: 7,
        flexDirection: 'column',
    },
    title: {
        fontSize: 24,
        marginTop: 30,
    },
    textInputContainer: {
        marginTop: 10,
        backgroundColor: "white",
        borderRadius: 5
    },
    textInput: {
        width: "90%",
        height: 40,
        paddingLeft: 10
    },
    khung_button: {
        marginTop: 50,

    },
    button_dangnhap: {
        backgroundColor: "white",
        width: 270, height: 40,
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 10,
        borderRadius: 10,
    },
    button_dangky: {
    },
    text_dangky: {
        color: "red",
    }

})