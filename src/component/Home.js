import React, { Component } from 'react';
import { View, ImageBackground, Text, TouchableOpacity, StyleSheet } from 'react-native';
import resource from '../data_/index';

export default class Home extends Component {
  render() {
    return (
      <View style={styles.container}>
        <ImageBackground source={resource.images.bghoingu} style={styles.bghoingu}>
            <ImageBackground source={resource.images.bg_khungcauhoi} resizeMode= 'stretch' style={styles.bg_khungcauhoi}>
              <Text style={styles.text_khung}>Chơi ngay đi</Text>
            </ImageBackground>
          <ImageBackground source={resource.images.i_9} style={styles.meo}></ImageBackground>
          <TouchableOpacity  
            onPress={()=>this.props.navigation.navigate('PlayGame')}
            style={styles.button_start}
          >
            <Text>CHƠI GAME</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}>
              <Text>Bảng phong thánh</Text>
          </TouchableOpacity>
          <TouchableOpacity  style={styles.button}><Text>Đánh giá ứng dụng</Text>
          </TouchableOpacity>
          <TouchableOpacity  style={styles.button}><Text>Fanpage</Text>
          </TouchableOpacity>
        </ImageBackground>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container:{
    width: "100%",justifyContent:"center", alignItems:"center", marginTop:0 
  },
  bghoingu:{
    width: '100%', height:"100%",justifyContent:"center", alignItems:"center" 
  },
  bg_khungcauhoi:{
    width:253,height:120,justifyContent:"center", alignItems:"center" ,
  },
  text_khung:{
    fontSize:25, color:"white",marginTop:-20
  },
  meo:{
    width:100,height:100,justifyContent:"center", alignItems:"center" ,marginLeft: -120, marginTop:-20
  },
  button_start:{
    backgroundColor:"white", width:250,height:40,justifyContent:"center", alignItems:"center",marginBottom:10, marginTop:10 ,borderRadius:20
  },
  button:{
    backgroundColor:"white", width:220,height:40,justifyContent:"center", alignItems:"center",marginBottom:10,borderRadius:20
  }
})