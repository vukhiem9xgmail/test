import React, { Component } from 'react';
import { View, ImageBackground, Text, TouchableOpacity, StyleSheet } from 'react-native';
import resource from '../data_/index';
import Toast from 'react-native-simple-toast';
import Tts from 'react-native-tts';
import AppDataManager from '../AppDataManager/AppDataManager';
var QuestionJson = require('../data_/data/hoingu.json');

const appDataManager = AppDataManager.getAppDataManagerInstance();

export default class PlayGame extends Component {
  constructor(props) {
    super(props);
    const appData = appDataManager.getAppData();
    this.state = appData;
  }
  componentDidMount() {
    appDataManager.addObserver(this);
    var _rand = QuestionJson[(Math.random() * QuestionJson.length) | 0];
    this.setState({ rand: _rand });
    this.Soune();
  }
  Go_Back = () => {
    this.props.navigation.navigate('Home');
  }
  Soune = () => {
    Tts.getInitStatus().then(() => {
      Tts.setDefaultLanguage('vi');
      Tts.setDefaultRate(0.6);
      Tts.speak(this.state.rand.cauhoi);
    });
  }
  Data = () => {
    this.setState({ socau: this.state.socau + 1, });
    var _rand = QuestionJson[(Math.random() * QuestionJson.length) | 0];
    const appData = appDataManager.getAppData();
    appData.rand = _rand;
    this.setState({ rand: _rand });
    appDataManager.updateAppData(appData);
  }
  DataUpData = () => {
    this.setState({ socau: 1});
    this.setState({ lan_ngu: 5});
    var _rand = QuestionJson[(Math.random() * QuestionJson.length) | 0];
    this.setState({ rand: _rand });
    const appData = appDataManager.getAppData();
    appData.rand = _rand;
    appDataManager.updateAppData(appData);
  }
  A = () => {
    Toast.showWithGravity(this.state.rand.gt_a, Toast.LONG, Toast.BOTTOM);
    if (this.state.rand.dapandung === "a") {
      this.props.navigation.navigate('Next', { data: this.state.rand.gt_a });
      this.Data();
    }
    else {
      if (this.state.lan_ngu > 1) {
        var _rand = QuestionJson[(Math.random() * QuestionJson.length) | 0];
        this.setState({ rand: _rand });
        this.setState({ lan_ngu: this.state.lan_ngu - 1, })
      }
      else {
        this.DataUpData();
        this.props.navigation.navigate('GameOver')
      }
    }
  }
  B = () => {
    Toast.showWithGravity(this.state.rand.gt_b, Toast.LONG, Toast.BOTTOM);
    if (this.state.rand.dapandung === "b") {
      this.props.navigation.navigate('Next', { data: this.state.rand.gt_b });
      this.Data();
    }
    else {
      if (this.state.lan_ngu > 1) {
        var _rand = QuestionJson[(Math.random() * QuestionJson.length) | 0];
        this.setState({ rand: _rand });
        this.setState({ lan_ngu: this.state.lan_ngu - 1, })
      }
      else {
        this.DataUpData();
        this.props.navigation.navigate('GameOver')
      }
    }
  }
  C = () => {
    Toast.showWithGravity(this.state.rand.gt_c, Toast.LONG, Toast.BOTTOM);
    if (this.state.rand.dapandung === "c") {
      this.props.navigation.navigate('Next', { data: this.state.rand.gt_c });
      this.Data();
    }
    else {
      if (this.state.lan_ngu > 1) {
        var _rand = QuestionJson[(Math.random() * QuestionJson.length) | 0];
        this.setState({ rand: _rand });
        this.setState({ lan_ngu: this.state.lan_ngu - 1, })
      }
      else {
        this.DataUpData();
        this.props.navigation.navigate('GameOver')
      }
    }
  }
  D = () => {
    Toast.showWithGravity(this.state.rand.gt_d, Toast.LONG, Toast.BOTTOM);
    if (this.state.rand.dapandung === "d") {
      this.props.navigation.navigate('Next', { data: this.state.rand.gt_d });
      this.Data();
    }
    else {
      if (this.state.lan_ngu > 1) {
        var _rand = QuestionJson[(Math.random() * QuestionJson.length) | 0];
        this.setState({ rand: _rand });
        this.setState({ lan_ngu: this.state.lan_ngu - 1, })
      }
      else {
        this.DataUpData();
        this.props.navigation.navigate('GameOver')
      }
    }
  }
  Test = () => {
      const chuoi = this.state.rand.cauhoi.length;
      console.log("rand", chuoi);
    
  }
  render() {
    const { rand, socau, lan_ngu } = this.state;
    var listmeo = [resource.images.i_1,resource.images.i_2,resource.images.i_3,resource.images.i_4,resource.images.i_5,
      resource.images.i_6,resource.images.i_7,resource.images.i_8,resource.images.i_9,resource.images.i_10];
      var meo = Math.floor(Math.random() * 10);
    return (
      <View>
        <ImageBackground source={resource.images.bghoingu} style={styles.bghoingu}>
          <View style={styles.khung_button}>
            <TouchableOpacity onPress={this.Go_Back}>
              <ImageBackground source={resource.images.ic_back1x} style={styles.back} />
            </TouchableOpacity>
            <View style={styles.right} >
              <TouchableOpacity onPress={this.Soune}>
                <ImageBackground source={resource.images.unSoune} style={styles.button_setting} />
              </TouchableOpacity>
              <TouchableOpacity onPress={this.Test}>
                <ImageBackground source={resource.images.cogwheel} style={styles.button_setting} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.container}>
            <ImageBackground source={resource.images.bg_khungcauhoi} resizeMode="stretch" style={styles.bg_khungcauhoi}>
              
                <Text style={styles.text_khungtrue} >{rand.cauhoi}</Text>
            </ImageBackground>
            <View style={styles.ngang}>
              <ImageBackground source={listmeo[meo]} style={styles.meo} />
              <View style={{}}>
                <Text>Câu số :{socau}</Text>
                <Text>Của:{rand.nickname}</Text>
                <Text>Lần ngu:{lan_ngu}</Text>
              </View>
            </View>
            <View style={{ flexDirection: "column",marginLeft:"6%", }}>
              <View style={{ flexDirection: "row" }}>
                <TouchableOpacity
                  onPress={this.A}
                  style={styles.button}><Text>{rand.a}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={this.B}
                  style={styles.button}><Text>{rand.b}</Text>
                </TouchableOpacity>
              </View>
              <View style={{ flexDirection: "row" }}>
                <TouchableOpacity
                  onPress={this.C}
                  style={styles.button}><Text>{rand.c}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={this.D}
                  style={styles.button}><Text>{rand.d}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    width: "100%", justifyContent: "center", alignItems: "center",
  },
  khung_button: {
    flexDirection: "row", marginTop: 20, marginBottom: 50
  },
  back: {
    width: 20, height: 20, marginLeft: "15%",
  },
  button_setting: {
    width: 20, height: 20, marginLeft: 7
  },
  right: {
    flexDirection: "row", justifyContent: "flex-end", marginLeft: "60%"
  },
  bghoingu: {
    width: '100%', height: "100%",
  },
  bg_khungcauhoi: {
    width: "90%", height: 169.8, justifyContent: "center", alignItems: "center",marginLeft:"7%"
  },
  text_khungtrue: {
    width:"75%",
    fontSize: 20, color: "white", marginTop: "-15%", marginLeft: "-3%"
  },
  text_khungfalse: {
    width:"75%",
    fontSize: 16, color: "white", marginTop: "-15%", marginLeft: "-3%"
  },
  ngang: {
    flexDirection: "row", marginBottom: 30 
  },
  meo: {
    width: 100, height: 100, justifyContent: "center", alignItems: "center", 
    marginTop: -20, marginRight: 100
  },
  button: {
    backgroundColor: "white", width: "45%", height: 50, justifyContent: "center",
    alignItems: "center", marginRight: 10, marginBottom: 10, borderRadius: 5
  },
})