var QuestionJson = require('../data_/data/hoingu.json');
export default class AppDataManager {
    static AppDataManagerInstance = null;
    appData = {
        rand:[],
        socau: 1,
        lan_ngu:5,
        rand_cu:[],
        data:[],
    };
    Observers = [];
    addObserver(observer) {
        this.Observers[this.Observers.length] = observer
    }
    removeObserver(o) {
        this.Observers = this.Observers.filter((observer) => observer != o);
    }
    static getAppDataManagerInstance() {
        if (AppDataManager.AppDataManagerInstance == null) {
            AppDataManager.AppDataManagerInstance = new AppDataManager();
            this.AppDataManagerInstance.loadDataFromLocal();
        }
        return this.AppDataManagerInstance;
    }
    getAppData() {
        return this.appData;
    }
    updateAppData(appData) {
        this.appData = appData;
        this.updateDataToLocal();
    }
    
    deleteAppData() {
        this.appData = {
            
        }
        this.updateDataToLocal();
    }
    updateDataToLocal() {
        // localStorage.removeItem("AppData")
        // localStorage.setItem("AppData", JSON.stringify(this.appData))
        // this.listenChange()
    }
    loadDataFromLocal() {
        // this.rand = QuestionJson[(Math.random() * QuestionJson.length) | 0];
        this.listenChange()
    }
    listenChange() {
        this.Observers.map(
            (o, index, arr) => {
                if (o.onAppDataChanged) {
                    o.onAppDataChanged(this)
                }
            }
        )
    }
}