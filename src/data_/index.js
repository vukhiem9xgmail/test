import add from './images/add.png';
import addButton from './images/addButton.png';
import bg_khungcauhoi from './images/bg_khungcauhoi.png';
import bghoingu from './images/bghoingu.png';
import close from './images/close.png';
import cogwheel from './images/cogwheel.png';
import cup from './images/cup.png';
import i_1 from './images/i_1.png';
import i_2 from './images/i_2.png';
import i_3 from './images/i_3.png';
import i_4 from './images/i_4.png';
import i_5 from './images/i_5.png';
import i_6 from './images/i_6.png';
import i_7 from './images/i_7.png';
import i_8 from './images/i_8.png';
import i_9 from './images/i_9.png';
import i_10 from './images/i_10.png';
import ic_back1x from './images/ic_back1x.png';
import ic_back2x from './images/ic_back2x.png';
import ic_back3x from './images/ic_back3x.png';
import ic_e_5 from './images/ic_e_5.png';
import unSoune from './images/unSoune.png';
import thief from './images/thief.png';




const resource = {
    images: {
        add,addButton,bg_khungcauhoi,bghoingu,close,cogwheel,cup,i_1,i_2, i_3, i_4, i_5, i_6, i_7, i_8, i_9, i_10,ic_back1x ,ic_back2x, ic_back3x,ic_e_5, unSoune,
        thief,
    }
}
export default resource;